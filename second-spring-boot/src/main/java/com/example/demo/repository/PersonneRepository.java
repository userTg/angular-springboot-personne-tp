package com.example.demo.repository;



import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.model.Personne;

// TODO: Auto-generated Javadoc
/**
 * The Interface PersonneRepository.
 */
public interface PersonneRepository extends JpaRepository<Personne, Long> {
	
	/**
	 * Chercher.
	 *
	 * @param mc the mc
	 * @param pageable the pageable
	 * @return the page
	 */
	@Query("select p from Personne p where p.nom like %:x%")
	public Page<Personne> chercher(@Param("x")String mc, Pageable pageable);
	

	/**
	 * Find by num.
	 *
	 * @param num the num
	 * @return the personne
	 */
	Personne findByNum(Long num);
}
