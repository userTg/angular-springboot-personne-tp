package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.model.Personne;
import com.example.demo.repository.PersonneRepository;


// TODO: Auto-generated Javadoc
/**
 * The Class PersonneRestControllerTest.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
class PersonneRestControllerTest {

	/** The personne repository. */
	@Autowired
	private PersonneRepository personneRepository;

	/** The entity manager. */
	@Autowired
	private TestEntityManager entityManager;

	/**
	 * Test get personnes.
	 */
	@Test
	void testGetPersonnes() {
		Personne p1 = new Personne("Dalton", "Joe");
		Personne p2 = new Personne("Luke", "Lucky");
		entityManager.persist(p1);
		entityManager.persist(p2);
		List<Personne> allPersonnesFromDb = personneRepository.findAll();
		List<Personne> personneList = new ArrayList<>();
		for (Personne personne : allPersonnesFromDb) {
			personneList.add(personne);
		}
		assertThat(personneList.size()).isEqualTo(2);

	}

	/**
	 * Test get personne by id.
	 */
	@Test
	void testGetPersonneById() {
		Personne personne = new Personne("admin", "admin");
		Personne personneSavedInDb = entityManager.persist(personne);
		Personne personneFromDb = personneRepository.getOne(personneSavedInDb.getNum());
		assertEquals(personneSavedInDb, personneFromDb);
		assertThat(personneFromDb.equals(personneSavedInDb));
	}

	/**
	 * Test save.
	 */
	@Test
	void testSave() {
		Personne p1 = new Personne("Luke", "Lucky");
		Personne pSavInDb = entityManager.persist(p1);
		Personne pFromDb = personneRepository.getOne(pSavInDb.getNum());
		assertEquals(pSavInDb,pFromDb);
		assertThat(pFromDb.equals(pSavInDb));
	}

	/**
	 * Test update personne.
	 */
	@Test
	void testUpdatePersonne() {
		Personne personne = new Personne("admin", "admin");
		entityManager.persist(personne);
		Personne getFromDb = personneRepository.getOne(personne.getNum());
		getFromDb.setNom("admino");
		assertThat(getFromDb.getNom().equals(personne.getNom()));
	}

	/**
	 * Test delete personne.
	 */
	@Test
	void testDeletePersonne() {
		Personne p1 = new Personne("Dalton", "Joe");
		Personne p2 = new Personne("Luke", "Lucky");
		Personne persist = entityManager.persist(p1);
		entityManager.persist(p2);
		entityManager.remove(persist);
		List<Personne> allPersonnesFromDb = personneRepository.findAll();
		List<Personne> personneList = new ArrayList<>();
		for (Personne personne : allPersonnesFromDb) {
			personneList.add(personne);
		}
		assertThat(personneList.size()).isEqualTo(1);

	}

}
