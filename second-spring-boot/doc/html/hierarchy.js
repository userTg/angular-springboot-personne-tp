var hierarchy =
[
    [ "com.example.demo.model.Commentaire", "classcom_1_1example_1_1demo_1_1model_1_1_commentaire.html", null ],
    [ "com.example.demo.controller.CommentaireController", "classcom_1_1example_1_1demo_1_1controller_1_1_commentaire_controller.html", null ],
    [ "com.example.demo.repository.CommentaireRepositoryTest", "classcom_1_1example_1_1demo_1_1repository_1_1_commentaire_repository_test.html", null ],
    [ "com.example.demo.service.CommentaireService", "interfacecom_1_1example_1_1demo_1_1service_1_1_commentaire_service.html", [
      [ "com.example.demo.service.CommentaireServiceImpl", "classcom_1_1example_1_1demo_1_1service_1_1_commentaire_service_impl.html", null ]
    ] ],
    [ "com.example.demo.service.CommentaireServiceTest", "classcom_1_1example_1_1demo_1_1service_1_1_commentaire_service_test.html", null ],
    [ "com.example.demo.FirstSpringBootApplication", "classcom_1_1example_1_1demo_1_1_first_spring_boot_application.html", null ],
    [ "com.example.demo.repository.PersonneRepositoryTest", "classcom_1_1example_1_1demo_1_1repository_1_1_personne_repository_test.html", null ],
    [ "com.example.demo.controller.PersonneRestController", "classcom_1_1example_1_1demo_1_1controller_1_1_personne_rest_controller.html", null ],
    [ "com.example.demo.service.PersonneService", "interfacecom_1_1example_1_1demo_1_1service_1_1_personne_service.html", [
      [ "com.example.demo.service.PersonneServiceImpl", "classcom_1_1example_1_1demo_1_1service_1_1_personne_service_impl.html", null ]
    ] ],
    [ "com.example.demo.service.PersonneServiceTest", "classcom_1_1example_1_1demo_1_1service_1_1_personne_service_test.html", null ],
    [ "RuntimeException", null, [
      [ "com.example.demo.exceptions.ResourceNotFoundException", "classcom_1_1example_1_1demo_1_1exceptions_1_1_resource_not_found_exception.html", null ]
    ] ],
    [ "JpaRepository", null, [
      [ "com.example.demo.repository.CommentaireRepository", "interfacecom_1_1example_1_1demo_1_1repository_1_1_commentaire_repository.html", null ],
      [ "com.example.demo.repository.PersonneRepository", "interfacecom_1_1example_1_1demo_1_1repository_1_1_personne_repository.html", null ]
    ] ],
    [ "Serializable", null, [
      [ "com.example.demo.dto.CommentaireDto", "classcom_1_1example_1_1demo_1_1dto_1_1_commentaire_dto.html", null ],
      [ "com.example.demo.dto.PersonneDto", "classcom_1_1example_1_1demo_1_1dto_1_1_personne_dto.html", null ],
      [ "com.example.demo.model.Personne", "classcom_1_1example_1_1demo_1_1model_1_1_personne.html", null ]
    ] ]
];